﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] private int maxHealth = 5;
    [SerializeField] private int speed = 300;
    [SerializeField] private AudioClip deathSfx;

    new private SpriteRenderer renderer;
    new private Rigidbody2D rigidbody;
    private float colorT = 0;
    private Quaternion lastRotation = Quaternion.Euler(0, 0, 90);

    private int actualHealth;

	// Use this for initialization
	void Start () {
        actualHealth = maxHealth;
        renderer = GetComponent<SpriteRenderer>();
        rigidbody = GetComponent<Rigidbody2D>();
	}

    void Update ()
    {
        renderer.color = Color.Lerp(Color.white, Color.red, colorT);
        colorT = Mathf.Max(0, colorT - .2f);
    }
	
	void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == (int) Layers.PLAYER_BULLETS)
        {
            actualHealth -= 1;
            colorT = 1;
        }
        if (actualHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void MoveLooselyTo(Vector3 point)
    {
        Quaternion lookDirection = Quaternion.LookRotation(point - transform.position);
        Vector3 direction = Quaternion.RotateTowards(lastRotation, lookDirection, 3) * Vector3.forward;
        lastRotation = Quaternion.LookRotation(direction);

        rigidbody.velocity = speed * direction * Random.Range(.85f,1);
    }
}
