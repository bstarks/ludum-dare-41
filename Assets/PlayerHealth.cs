﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    [SerializeField] private int maxHealth = 5;
    [SerializeField] private AudioClip deathSfx;
    [SerializeField] private int actualHealth;

    // Use this for initialization
    void Start()
    {
        actualHealth = maxHealth;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == (int)Layers.PLAYER_BULLETS)
        {
            actualHealth -= 1;
        }
        if (actualHealth <= 0)
        {
            Application.Quit();
        }
    }
}
