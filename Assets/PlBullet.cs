﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlBullet : MonoBehaviour {

    [SerializeField] private float speed = 1000f;
    [SerializeField] private int timeToLive = 300;
    [SerializeField] private AudioClip hitSound;

    new private AudioSource audio;
    new private SpriteRenderer renderer;
    new private Collider2D collider;

    void Start ()
    {
        audio = GetComponent<AudioSource>();
        renderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<Collider2D>();
    }

	// Update is called once per frame
	void Update () {
        timeToLive -= 1;
        if (timeToLive <= 0)
        {
            //Destroy when TTL reaches zero
            //I don't want to add pooling for a game jam
            Destroy(gameObject);
        }
        transform.Translate(Vector3.up * speed * Time.deltaTime);
	}

    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        if (hitSound && otherCollider.gameObject.layer == (int) Layers.ENEMIES)
        {
            audio.PlayOneShot(hitSound, 1);
        }

        //Hide active components and destroy object later
        collider.enabled = false;
        renderer.enabled = false;
        timeToLive = 15;
    }
}
