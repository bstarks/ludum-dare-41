﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {
    
    [SerializeField] private Transform target;
    [SerializeField] private float dampTime = 0.015f;

    private Vector3 velocity = Vector3.zero;


    // Update is called once per frame
    // Smooth camera moverment from https://answers.unity.com/questions/29183/2d-camera-smooth-follow.html
    void Update () {
        if (target)
        {
            Vector3 cameraViewportPosition = Camera.main.WorldToViewportPoint(target.position);
            Vector3 delta = target.position - Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, cameraViewportPosition.z)); //Vector from middle of screen to target
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
            transform.position = new Vector3((int) transform.position.x, (int) transform.position.y, (int) transform.position.z);
        }	
	}
}
