﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Layers
{
    DEFAULT,
    TRANSPARENT_FX,
    IGNORE_RAYCAST,
    LAYER_3,
    WATER,
    UI,
    LAYER_6,
    LAYER_7,
    PLAYER,
    PLAYER_BULLETS,
    ENEMIES,
    ENEMY_BULLETS,
    STAGE
}