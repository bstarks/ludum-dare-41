﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    [SerializeField] private float maxSpeed = 750f;
    [SerializeField] private float rotationSpeed = 135f;
    [SerializeField] private int shootDelay = 5;
    [SerializeField] private AudioClip crashSfx;
    [SerializeField] private AudioClip laserSfx;
    [SerializeField] private GameObject bulletPrefab;

    new private Rigidbody2D rigidbody;
    private AudioSource bulletSoundSource;
    private AudioSource carSoundSource;
    new private SpriteRenderer renderer;

    private const float SOUND_PADDING = 50f;

    private bool upPress;
    private bool leftPress;
    private bool rightPress;
    private bool guardBrakePress;
    private bool shootPress;
    private bool downPress;
    private float actualSpeed = 0;
    private float shootTimer = 0;
    private int health = 20;
    private float colorT = 0;

    public int lapNum = 1;
    public bool isDead = false;
    private int nextFinishLine = 1;

    void Start ()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        var audioSources = GetComponents<AudioSource>();
        bulletSoundSource = audioSources[0];
        carSoundSource = audioSources[1];
        renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update ()
    {
        renderer.color = Color.Lerp(Color.white, Color.red, colorT);
        colorT = Mathf.Max(0, colorT - .2f);

        //Check button input
        upPress = Input.GetKey(KeyCode.UpArrow);
        leftPress = Input.GetKey(KeyCode.LeftArrow);
        rightPress = Input.GetKey(KeyCode.RightArrow);
        guardBrakePress = Input.GetKey(KeyCode.X) || Input.GetKey(KeyCode.LeftShift);
        downPress = Input.GetKey(KeyCode.DownArrow);
        shootPress = Input.GetKey(KeyCode.Z);

        //Calculate actual speed
        //Car ramps up to top speed when moving forward
        //Car slows down to zero speed when not moving forward
        //Car slows down quicker when guarding/braking
        float speedIncrement = (0.033f) * maxSpeed;
        if (upPress && !guardBrakePress && !downPress)
        {
            actualSpeed += speedIncrement;
        }
        else if (downPress && !guardBrakePress)
        {
            actualSpeed -= speedIncrement;
        }
        else
        {
            actualSpeed -= guardBrakePress ? 2 * speedIncrement : speedIncrement;
            actualSpeed = Mathf.Max(0, actualSpeed);
        }
        actualSpeed = Mathf.Clamp(actualSpeed, - (maxSpeed / 3), maxSpeed);

        //Fire bullet
        shootTimer = Mathf.Max(0, shootTimer - 1);
        if (shootPress && bulletPrefab && (shootTimer == 0))
        {
            PlaySoundWithInterrupt(bulletSoundSource, laserSfx, .5f);
            Instantiate(bulletPrefab, transform.position + Vector3.Normalize(transform.TransformDirection(Vector3.up)), transform.rotation);
            shootTimer = shootDelay;
        }
    }
	
    //Update physics
	void FixedUpdate () {
        rigidbody.velocity = actualSpeed * transform.TransformDirection(Vector3.up); ;

        rigidbody.angularVelocity = leftPress ? rotationSpeed
                                    : rightPress ? -rotationSpeed
                                    : 0;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (crashSfx)
        {
            //Set volume to the 4th power of (force of collision / max force of collision (with padding to account for rotation))
            //This makes weaker hits sound even more quiet compared to harder hits
            float volume = collision.relativeVelocity.magnitude / (maxSpeed + SOUND_PADDING);
            volume = Mathf.Pow(volume,4);
            volume = Mathf.Clamp(volume - 0.1f, 0, 1);
            PlaySoundWithInterrupt(carSoundSource, crashSfx, volume);

            if (volume > .33f)
            {
                health--;
                colorT = 1;
                if (health <= 0)
                {
                    isDead = true;
                }
            }

            actualSpeed *= Mathf.Clamp(1 - (collision.relativeVelocity.magnitude / maxSpeed), 0, 1);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        int line = System.Int32.Parse(collider.gameObject.name);
        if (line == nextFinishLine)
        {
            nextFinishLine = (nextFinishLine + 1) % 4;
            if (nextFinishLine == 1)
            {
                lapNum += 1;
            }
        }
    }

    //End any playing sound effect and play a new one
    //This stops, for example, crash sound effects from playing on top of each other
    private void PlaySoundWithInterrupt(AudioSource audio, AudioClip audioClip, float volume)
    {
        audio.Stop();
        if (audioClip)
        {
            audio.PlayOneShot(audioClip, volume);
        }
    }

    private void StopSound(AudioSource audio, AudioClip audioClip)
    {
        if (audio.clip == audioClip)
        {
            audio.Stop();
        }
    }
}
