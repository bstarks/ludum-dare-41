﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameFlow : MonoBehaviour {

    [SerializeField] private float matchDistance = 50;
    [SerializeField] private Text text;
    

    GameObject enemies;
    List<Transform> waypoints;
    Dictionary<Transform, int> enemyTransformToWaypointMap;
    int numOfWaypoints;
    GameObject player;

    private float startTime;
    private float endTime;
    private bool endGame = false;
    private bool waitForRestart = false;

    
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        enemies = GameObject.Find("Enemies");
        waypoints = new List<Transform>();
        foreach (Transform transform in GameObject.Find("Waypoints").transform)
        {
            waypoints.Add(transform);
        }
        numOfWaypoints = waypoints.Count;

        enemyTransformToWaypointMap = new Dictionary<Transform, int>();
        foreach (Transform enemyTransform in enemies.transform)
        {
            enemyTransformToWaypointMap.Add(enemyTransform, Random.Range(0, numOfWaypoints));
        }

        startTime = Time.time;
        
    }
	
	// Update is called once per frame
	void Update () {

        if (waitForRestart)
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                Time.timeScale = 1;
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            return;
        }
        if (endGame)
        {
            Time.timeScale = 0;
            if (endTime > 0)
            {
                float totalTime = endTime - startTime;
                text.text = Judge(totalTime) + "\n\n\n\nPress down arrow to play again";
            }
            else
            {
                text.text = "You died" + "\n\n\n\nPress down arrow to play again";
            }
            
            waitForRestart = true;
            return;
        }

		foreach (Transform enemyTransform in enemies.transform) {
            if (enemyTransform)
            {
                Enemy enemy = enemyTransform.GetComponent<Enemy>();
                Transform waypoint = waypoints[enemyTransformToWaypointMap[enemyTransform]];
                enemy.MoveLooselyTo(waypoint.position);

                if ((waypoint.position - enemyTransform.position).magnitude < matchDistance)
                {
                    enemyTransformToWaypointMap[enemyTransform] = Random.Range(0, numOfWaypoints);
                }
            }
        }

        if (player.GetComponent<Player>().lapNum == 4)
        {
            endTime = Time.time;
            endGame = true;
        }

        if (player.GetComponent<Player>().isDead)
        {
            endGame = true;
        }
	}

    private string Judge(float time)
    {
        string returnVal = time + " seconds: ";
        if (time > 30)
        {
            returnVal += "You can do better";
        }
        if (time <= 30 && time > 24)
        {
            returnVal += "Not terrible";
        }
        if (time <= 24 && time > 20)
        {
            returnVal += "That's pretty good";
        }
        if (time <= 20 && time > 15)
        {
            returnVal += "You're amazing!";
        }
        if (time < 15)
        {
            returnVal += "You cheated";
        }
        return returnVal;
    }
}
